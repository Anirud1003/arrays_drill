const filter = require("../filter.cjs");
const array = require("../array.cjs");

const result = array.filter((num) => {
  return num >= 3;         // return > 5; Return an empty array when no elements pass the truth test
});
console.log(result);
