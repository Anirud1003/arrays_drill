function flatten(array, depth) {
  if (!Array.isArray(array)) {
    return [];
  }
  if (depth === undefined) {
    depth = 1;
  }
  let flatArray = [];
  for (let index = 0; index < array.length; index++) {
    if (Array.isArray(array[index]) && depth > 0) {
      flatArray = flatArray.concat(flatten(array[index], depth - 1));
    } else if (array[index] === undefined || array[index] === null) {
      continue;
    } else {
      flatArray.push(array[index]);
    }
  }
  return flatArray;
}
module.exports = flatten;
