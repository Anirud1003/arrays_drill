function find(elements, callback) {
  if (!Array.isArray(elements) || !callback) {
    return [];
  } else {
    for (let index = 0; index < elements.length; index++) {
      if (callback(elements[index])) {
        return elements[index];
      }
    }
  }
}
module.exports = find;
