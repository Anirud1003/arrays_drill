function reduce(elements, callback, startingValue) {
    if (!Array.isArray(elements)) {
        return 0;
    } else {
        if (elements.length === 0 && startingValue === undefined) {
            return undefined;
        }
        if (elements.length === 0 && startingValue !== undefined) {
            return startingValue;
        }

        if (startingValue == undefined) {
            startingValue = elements[0]
            let index = 1;

            for (index = 1; index < elements.length; index++) {

                startingValue = callback(startingValue, elements[index], index, elements)

            }
        }
        else {
            let index = 0

            for (index = 0; index < elements.length; index++) {

                startingValue = callback(startingValue, elements[index], index, elements)

            }
        }

    }
    return startingValue
}
module.exports = reduce;

