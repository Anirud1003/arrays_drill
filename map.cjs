function map(elements, callback) {
    if (!Array.isArray(elements) || !callback) {
        return undefined;
    }
    else {
        let number = [];
        for (let index = 0; index < elements.length; index++) {
            number.push(callback(elements[index], index, elements))
        }
        return number
    }
}
module.exports = map
