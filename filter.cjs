function filter(elements, callback) {
  if (Array.isArray(elements)) {
    if (elements.length == 0) {
      return [];
    } else {
      let ans = [];
      for (let index = 0; index < elements.length; index++) {
        if (callback(elements[index], index, elements) == true) {
          ans.push(elements[index]);
        }
      }
      return ans;
    }
  } else {
    return 0;
  }
}
module.exports = filter;
