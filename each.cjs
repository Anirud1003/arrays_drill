function each(arrays, callback) {
    if (!Array.isArray(arrays) || !callback) {
        return undefined;
    }
    else {
        for (let index = 0; index < arrays.length; index++) {
            console.log(callback(arrays[index], index));
        }

    }
}
module.exports = each
